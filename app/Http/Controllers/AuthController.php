<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
    	return view('register');
    }

    public function welcome() {
    	return view('welcome');
    }

	public function welcomePost(Request $request ) {
    	// $request ->all();
		$nama1 = $request["sfname"];
		$nama2 = $request["slname"];
		return view('welcome',['nama1' => $nama1],['nama2' => $nama2]);
    }
}
