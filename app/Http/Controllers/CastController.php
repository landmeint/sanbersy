<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function create() {
        return view('casts.create');
    }
    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        // $query = DB::table('casts')->insert([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);

        // $cast = new Cast;
        // $cast->nama = $request->nama;
        // $cast->umur = $request->umur;
        // $cast->bio = $request->bio;
        // $cast->save(); 
        
        $cast = Cast::create([
            "nama" => $request["nama"],
             "umur" => $request["umur"],
             "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('Success','Berhasil Disimpan');
    }
    public function index() {
        // $casts = DB::table('casts')->get();
        $casts = Cast::all();
        // dd($casts);
        $casts = Cast::all();
        return view('casts.index', compact('casts'));
    }
    public function show($id) {
        // $showId = DB::table('casts')->where('id',$id)->first();
        $showId = Cast::find($id);
        // dd($showId);
        return view('casts.show', compact('showId'));
    }
    public function edit($id) {
        // $showId = DB::table('casts')->where('id',$id)->first();
        $showId = Cast::find($id);
        return view('casts.edit', compact('showId'));

    }
    public function update($id, Request $request) {
        // $request->validate([
        //     'nama' => 'required|unique:casts',
        //     'umur' => 'required',
        //     'bio' => 'required'
        // ]);
        // $query = DB::table('casts')
        //             ->where('id',$id)
        //             ->update([
        //                 'nama' => $request['nama'],
        //                 'umur' => $request['umur'],
        //                 'bio' => $request['bio']
        //             ]);

        $update = Cast::where('id',$id)->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
        ]);
        return redirect('/cast')->with('Success','Berhasil Update');
    }
    public function destroy($id)
    {
        // $query = DB::table('casts')->where('id',$id)->delete();
        Cast::destroy($id);
        return redirect('/cast')->with('Success','Data Berhasil Dihapus');
    }
}
