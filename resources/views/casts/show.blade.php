@extends('layouts.master')
@section('content')
<div class="mx-2 mt-2">
    <h3> {{ $showId->nama }} - {{ $showId->umur }}th </h3>
    <p> {{ $showId->bio }} </p>
</div>
@endsection