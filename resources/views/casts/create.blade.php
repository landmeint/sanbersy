@extends('layouts.master')
@section('content')
<div class="mx-2 mt-2">
  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title">Create New Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast" method="POST">@csrf
      <div class="card-body">
        <div class="form-group">
          <label for="namaCast">Nama</label>
          <input type="text" class="form-control" id="namaCast" name="nama" value=" {{ old('nama','') }} " placeholder="Masukkan Nama">
          @error('nama')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
            <label for="umurCast">Umur</label>
            <input type="text" class="form-control" id="umurCast" name="umur" value=" {{ old('umur','') }} " placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bioCast">Bio</label>
            <textarea class="form-control" id="bioCast" name="bio" value=" {{ old('bio','') }} " rows="3"></textarea>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
      </div>

      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-info">Create</button>
      </div>
    </form>
  </div>
</div>    
@endsection