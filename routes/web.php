<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::get('/welcome', 'AuthController@welcome');
Route::get('/master', function () {
         return view('layouts/master');
});
Route::get('/table', function () {
    return view('contents/table');
});
Route::get('/data-tables', function () {
    return view('contents/datatables');
});
Route::post('/welcome', 'AuthController@welcomePost');

// Route::get('/cast/create', 'CastController@create');
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/{id}', 'CastController@show');
// Route::get('/cast/{id}/edit', 'CastController@edit');
// Route::post('/cast', 'CastController@store');
// Route::put('/cast/{id}', 'CastController@update');
// Route::delete('/cast/{id}', 'CastController@destroy');


// resource method
Route::resource('cast', 'CastController');